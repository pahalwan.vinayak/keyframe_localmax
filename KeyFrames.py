"""
This key frame extract algorithm is based on interframe difference.
First, we load the video and compute the interframe difference between each frames.
Then frames which the average interframe difference are local maximum are considered to be key frames.
"""
import cv2
import operator
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
import gc
import psutil, os, sys
from time import time
import logging
logger = logging.getLogger()
logger.addHandler(logging.StreamHandler(sys.stdout))

def smooth(x, window_len=13, window='hanning'):
    logger.info("length of frames: %d" % len(x))
    s = np.r_[2 * x[0] - x[window_len:1:-1], x, 2 * x[-1] - x[-1:-window_len:-1]]
    if window == 'flat':
        w = np.ones(window_len, 'd')
    else:
        w = getattr(np, window)(window_len)
    y = np.convolve(w / w.sum(), s, mode='same')
    return y[window_len - 1:-window_len + 1]

class Frame():
    def __init__(self, id, diff):
        self.id, self.diff = id, diff


def rel_change(a, b):
   x = (b - a) / max(a, b)
   logger.debug(x)
   return 


class KeyFrames():
    USE_TOP_ORDER = 1  # Setting fixed threshold criteria
    USE_THRESH = 2
    USE_LOCAL_MAXIMA = 4
    def __init__(self, mode = 4, debug = False, **kwargs):
        #Constant

        self.set(mode, debug, **kwargs)

    def set(self, mode, debug = -1, **kwargs):
        if mode:
            self.use_top_order = 1 & mode
            self.use_thresh = 2 & mode
            self.use_local_maxima = 4 & mode
            if self.use_thresh:
                logger.info("Use Threshold")
                self.thresh = kwargs.get("thresh", 0.6)
            if self.use_top_order:
                logger.info("Use top order")
                self.num_top_frames = kwargs("num_top_frames", 50)
            if self.use_local_maxima:
                self.len_window = kwargs.get("len_window", 50)
                logger.info("Use Local Maxima")
        if debug == -1:
            pass
        elif debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

    def extract(self, video_path, dir=None, mode=0, debug = -1, **kwargs):
        self.set(mode, debug, **kwargs)

        start_time = time()
        logger.debug(sys.executable)
        if not dir:
            dir = os.path.splitext(video_path)[0]
            if not os.path.exists(dir):
                os.mkdir(dir)

        logger.debug("initial allocated memory: %d MB" % (psutil.Process(os.getpid()).memory_info().rss >> 20))
        logger.info("target video: " + os.path.abspath(video_path))
        logger.info("frame save directory: " + os.path.abspath(dir))

        # load video and compute diff between frames
        cap = cv2.VideoCapture(str(video_path))
        curr_frame = None
        prev_frame = None
        frame_diffs = []
        frames = []
        success, frame = cap.read()
        i = 0
        while(success):
            luv = cv2.cvtColor(frame, cv2.COLOR_BGR2LUV)
            curr_frame = luv
            if curr_frame is not None and prev_frame is not None:
                #logic here
                diff = cv2.absdiff(curr_frame, prev_frame)
                diff_sum = np.sum(diff)
                diff_sum_mean = diff_sum / (diff.shape[0] * diff.shape[1])
                frame_diffs.append(diff_sum_mean)
                frame = Frame(i, diff_sum_mean)
                frames.append(frame)
            prev_frame = curr_frame
            i = i + 1
            success, frame = cap.read()
        cap.release()

        # compute keyframe
        keyframe_id_set = set()
        if self.use_top_order:
            # sort the list in descending order
            frames.sort(key=operator.attrgetter("diff"), reverse=True)
            for keyframe in frames[:self.num_top_frames]:
                keyframe_id_set.add(keyframe.id)
        if self.use_thresh:
            for i in range(1, len(frames)):
                if (rel_change(np.float(frames[i - 1].diff), np.float(frames[i].diff)) >= self.thresh):
                    keyframe_id_set.add(frames[i].id)
        if self.use_local_maxima:
            diff_array = np.array(frame_diffs)
            sm_diff_array = smooth(diff_array, self.len_window)
            frame_indexes = np.asarray(argrelextrema(sm_diff_array, np.greater))[0]
            for i in frame_indexes:
                keyframe_id_set.add(frames[i - 1].id)

            plt.figure(figsize=(40, 20))
            #plt.locator_params(numticks=100)
            plt.stem(sm_diff_array)
            plt.savefig(os.path.join(dir, 'plot.png'))
            plt.close('all'); gc.collect(); # these 2 statements will release the memory of plt and save 60MB

        cap = cv2.VideoCapture(str(video_path))
        curr_frame = None
        keyframes = []
        success, frame = cap.read()
        idx = 0

        while(success):
            if idx in keyframe_id_set:
                path_1 = os.path.abspath(dir)
                file_1 = os.path.basename(path_1)
                name = "%s_keyframe_%04d.jpg" %(file_1,idx)
                cv2.imwrite(os.path.join(dir, name), frame)
                keyframe_id_set.remove(idx)
            idx = idx + 1
            success, frame = cap.read()

        cap.release()

        end_time = time()
        logger.info("elapsed time: %d" % (end_time - start_time))
        logger.debug("end allocated memory: %dMB" % (psutil.Process(os.getpid()).memory_info().rss >> 20))

if __name__ == "__main__":
    keyframes = KeyFrames()
    for root, dirs, files, in os.walk(r'C:/Users/path'):
        for filename in files:
            keyframes.extract("clips/"+filename)
