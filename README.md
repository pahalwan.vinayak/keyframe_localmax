This key frame extract algorithm is based on interframe difference.
First, we load the video and compute the interframe difference between each frames.
Then frames which the average interframe difference are local maximum are considered to be key frames.
